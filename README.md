# JupyterLite Demo

JupyterLite deployed as a static site to GitLab Pages, for demo purposes.

## ✨ Try it in your browser ✨

➡️ **https://benabel.gitlab.io/jupyterlite-template**

<!-- ![GitLab-pages](https://user-images.GitLabusercontent.com/591645/120649478-18258400-c47d-11eb-80e5-185e52ff2702.gif) -->

## Requirements

JupyterLite is being tested against modern web browsers:

- Firefox 90+
- Chromium 89+

## Usage

This repository is meant to demo how to deploy JupyterLite to GitLab Pages, using the released prebuilt JupyterLite assets.

For more info, keep an eye on the JupyterLite documentation:

- Configuring: https://jupyterlite.readthedocs.io/en/latest/configuring.html
- Deploying: https://jupyterlite.readthedocs.io/en/latest/deploying.html

### Deploy a new version of JupyterLite

To deploy a new version of JupyterLite, you can update the version in https://GitLab.com/jtpio/jupyterlite-template/blob/main/requirements.txt.

The `requirements.txt` file can also be used to add extra JupyterLab extensions to the deployed JupyterLite website.

Then commit and push the changes. The site will be deployed on the next push to the `main` branch.

## Development

Create a new environment:

```bash
mamba create -n jupyterlite-template
conda activate jupyterlite-template
pip install -r requirements.txt
```

Then follow the steps documented in the [Configuring](https://jupyterlite.readthedocs.io/en/latest/configuring.html) section.
